const request = require('request')
const requestp = require('request-promise-native');
const express = require('express');
const unavo = require("./helpers/unavo"); 
const bodyParser = require('body-parser');
const checkRoute = require("./routes/checkRoute");

const port = process.env.PORT || 3000;
const app = express();

// app.post('/checkRoute/CheckBalance', function(req, res) {
//     res.send(req.params.version);
//   });


//middlewares
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use("/checkRoute", checkRoute);

const options = {
    method: "POST",
    headers: {
        "Cache-Control": "no-cache",
        "Content-Type": "application/json"
    },
    body: {
        PrepaidWirelessCredential: {
            USERNAME: "North American Local",
            TOKENPASSWORD: "qAWufANapEye8uS",
            PIN: "North American Local5691",
            CLEC: "1004"
        }
    },
    json: true
}; 






//GET INFO ABOUT SERVICE (HTTP)
// app.get(["/getServiceInfo", "/getBalance"], (req, res) => {
//     let plan = req.query;

//     let optionsInternal = Object.assign({}, options);
//     optionsInternal.body.MDN = plan.MDN;
//     optionsInternal.url = "http://prepaidwirelesswebapi.fasimo.com/PrepaidWireless/GetServiceInfo";

//     requestp(optionsInternal).then(body => {
//         if (body.BalanceDetail.SubscriberState === null) res.json({ Status: "Not a valid number" });
//         let BalanceDetail = body.BalanceDetail;
//         let obj = {};
//         obj.Status = BalanceDetail.SubscriberState;
//         obj.TalkMinutes = BalanceDetail.Talk;
//         obj.TextMessages = BalanceDetail.Text;
//         obj.Data = BalanceDetail.Data;
//         obj.EndDate = BalanceDetail.ValidTo;
//         obj.Balance = BalanceDetail.Balance;
//         res.json(obj);
//     });
// });



app.listen(port, () => console.log("Check Balance App is Running on", port,));