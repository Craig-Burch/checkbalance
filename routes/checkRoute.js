const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const unavo = require("../helpers/unavo"); 
const validation = require("../validator/checkRoute"); 

router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
}); 



router.get("/checkBalance", bodyParser.json());
router.get("/checkBalance", validation.checkBalance);

router.get("/checkBalance", (req, res) => {

    unavo
        .checkBalance(req.body)
        .then(data => {
           if (data.Status) return Promise.fulfilled("Success!");
           res.status(200).json(data);
        })
        .then(data => {
            if (!data.Status) return Promise.reject("Error:" + data.ErrorMessage);
            res.status(200).json(data);

        })
        .catch(err => {
            console.log("Error when checking balance ", err);
            res.status(500).json(err);
        });
});


module.exports = router;



  // .then(data => {
  //     if (data.Status) return Promise.fulfilled("Success!");
  //     res.status(200).json(data);
  // })