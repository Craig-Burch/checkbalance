const checkBalance = (req, res, next) => {
    let err = [];
    const body = req.body;
    if (Object.keys(body).length === 0) err.push({ status: "Error", message: "Body is empty" });
    //customer
    if (!body) {
        err.push({ status: "Error", message: "customer is mandatory" });
        //console.log(body);
    }
    //esn
    if (!body.ESN) {
        err.push({ status: "Error", message: "customer ESN is mandatory" });
        //console.log(body.ESN);
    }
   
    //mdn
    if (!body.MDN) {
        err.push({ status: "Error", message: "customer MDN is mandatory" });
        //console.log(body.MDN); 
    }
  
    if (err.length > 0) {
        return res.status(400).json(err);
    }
    err = [];
    next();
};

module.exports = {
    checkBalance: checkBalance 
};